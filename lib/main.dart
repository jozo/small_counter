import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(const SmallCounterApp());
}

class SmallCounterApp extends StatelessWidget {
  const SmallCounterApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Small Counter',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.green),
        useMaterial3: true,
      ),
      home: const HomePage(title: 'Small Counter'),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key, required this.title});

  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<int> counters = [];

  @override
  void initState() {
    super.initState();
    loadSavedCounters();
  }

  Future<void> loadSavedCounters() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      var encodedCounters = prefs.getString('counters');
      if (encodedCounters == null) {
        counters = [0];
      } else {
        counters = [...jsonDecode(encodedCounters)["savedCounters"]];
      }
    });
  }

  Future<void> saveCounters() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString("counters", jsonEncode({"savedCounters": counters}));
  }

  void addCounter() {
    setState(() {
      counters.add(0);
    });
    saveCounters();
  }

  void increaseCounter(int index) {
    setState(() {
      var newCounters = [...counters];
      newCounters[index]++;
      counters = newCounters;
    });
    saveCounters();
  }

  void decreaseCounter(int index) {
    setState(() {
      var newCounters = [...counters];
      newCounters[index]--;
      counters = newCounters;
    });
    saveCounters();
  }

  void deleteCounter(int index) {
    setState(() {
      var newCounters = [...counters];
      newCounters.removeAt(index);
      counters = newCounters;
    });
    saveCounters();
  }

  List<DynamicCounter> createWidgets() {
    List<DynamicCounter> items = [];
    for (var i = 0; i < counters.length; i++) {
      items.add(DynamicCounter(
        count: counters[i],
        index: i,
        increase: increaseCounter,
        decrease: decreaseCounter,
        delete: deleteCounter,
      ));
    }
    return items;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ListView(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              children: createWidgets(),
            ),
            ElevatedButton(
                onPressed: addCounter, child: const Text("Add counter")),
          ],
        ),
      ),
    );
  }
}

typedef IncreaseCallback = Function(int index);
typedef DecreaseCallback = Function(int index);
typedef DeleteCounterCallback = Function(int index);

class DynamicCounter extends StatelessWidget {
  const DynamicCounter(
      {required this.count,
      required this.index,
      required this.increase,
      required this.decrease,
      required this.delete,
      super.key});

  final int count;
  final int index;
  final IncreaseCallback increase;
  final DecreaseCallback decrease;
  final DeleteCounterCallback delete;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ElevatedButton(
              onPressed: () => {decrease(index)},
              child: const Icon(Icons.remove)),
          Container(
            margin: const EdgeInsets.fromLTRB(15, 15, 15, 15),
            child: Text('$count',
                style: const TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                )),
          ),
          ElevatedButton(
              onPressed: () => {increase(index)}, child: const Icon(Icons.add)),
          Container(
            margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
            child: IconButton(
              onPressed: () => {delete(index)},
              icon: const Icon(Icons.delete),
            ),
          ),
        ],
      ),
    );
  }
}

